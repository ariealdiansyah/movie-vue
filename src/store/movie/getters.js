export const getListNowPlaying = (state) => {
  const data = state.listNowPlaying.slice(0, 8)
  return data
}
export const getListTopRated = (state) => {
  const data = state.listTopRated.slice(0, 8)
  return data
}
export const getListUpComing = (state) => {
  const data = state.listUpComing.slice(0, 8)
  return data
}
export const getListSimiliar = (state) => {
  const data = state.listSimiliar.slice(0, 8)
  return data
}
export const getDetails = (state) => {
  const data = state.details
  return data
}
